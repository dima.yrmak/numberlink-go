package server

import (
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/pbkdf2"
)

const (
	secretKey                = "1ba598e1efb9d0d6ae8e4b5dcd2171afa2dc3d1e7fcadefbef76561278da2ec1"
	salt                     = "fe8e1c4b551f32f1d52e47abe01cc7bd"
	accessTokenExpireMinutes = 30
	hashIterations           = 100000
	hashKeyLength            = 32
)

func getPasswordHash(password string) string {
	saltBytes, _ := hex.DecodeString(salt)
	hash := pbkdf2.Key([]byte(password), saltBytes, hashIterations, hashKeyLength, sha256.New)
	return hex.EncodeToString(hash)
}

func verifyPassword(plainPassword, hashedPassword string) bool {
	saltBytes, _ := hex.DecodeString(salt)
	hash := pbkdf2.Key([]byte(plainPassword), saltBytes, hashIterations, hashKeyLength, sha256.New)
	hashHex := hex.EncodeToString(hash)
	return subtle.ConstantTimeCompare([]byte(hashHex), []byte(hashedPassword)) == 1
}

func createAccessToken(data map[string]interface{}) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(time.Minute * accessTokenExpireMinutes).Unix(),
	})
	for key, value := range data {
		token.Claims.(jwt.MapClaims)[key] = value
	}
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
