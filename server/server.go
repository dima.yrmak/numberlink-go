package server

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"math/rand"
	"net/http"
	"numberlink/db"
	"numberlink/db/entities"
	"numberlink/src"
	"strings"
	"time"
)

func RunServer() {
	router := gin.Default()
	router.POST("/create-user", createUser)
	router.POST("/login", login)

	auth := router.Group("/")
	auth.Use(AuthMiddleware())
	{
		auth.GET("/board", getBoard)
		auth.POST("/board", loadBoard)
		auth.POST("/solve", solve)
	}

	corsConfig := cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}

	// Apply CORS middleware
	router.Use(cors.New(corsConfig))

	router.Run(":8000")
}

func createUser(c *gin.Context) {
	var userModel entities.User
	if err := c.BindJSON(&userModel); err != nil {
		c.Abort()
		return
	}

	user := db.GetUser(userModel.Email)
	if user == nil {
		c.JSON(200, db.InsertUser(&userModel))
		return
	}

	c.JSON(200, false)
}

func login(c *gin.Context) {
	var userModel entities.User
	if err := c.BindJSON(&userModel); err != nil {
		c.Abort()
		return
	}

	user := db.GetUser(userModel.Email)
	response := make(map[string]string)
	if user == nil {
		response["error"] = "User not found"
		c.JSON(400, response)
		return
	} else if !verifyPassword(userModel.Password, user.Password) {
		response["error"] = "Invalid password"
		c.JSON(400, response)
		return
	}

	data := make(map[string]interface{})
	data["sub"] = userModel.FullName

	response["access_token"], _ = createAccessToken(data)
	response["token_type"] = "bearer"

	c.JSON(200, response)
}

func getBoard(c *gin.Context) {
	flowsIds := db.GetFlowIds()

	response := make(map[string]interface{})
	if len(flowsIds) == 0 {
		response["error"] = "No grids in database"
		c.JSON(500, response)
		return
	}
	selectedFlowId := flowsIds[rand.Intn(len(flowsIds))]

	flow := db.GetFlowById(selectedFlowId)
	if flow == nil {
		response["error"] = "Grid not found"
		c.JSON(500, response)
		return
	}

	response["game_board"] = flow.Grid
	response["id"] = selectedFlowId
	c.JSON(200, response)
}

func loadBoard(c *gin.Context) {
	var model entities.NumberLinkModel
	if err := c.BindJSON(&model); err != nil {
		c.Abort()
		return
	}

	flow := db.GetFlowByGrid(model.Grid)
	if flow == nil {
		c.JSON(200, db.InsertFlow(flow))
		return
	}

	c.JSON(400, -1)
}

func solve(c *gin.Context) {
	var model entities.NumberLinkModel
	if err := c.BindJSON(&model); err != nil {
		c.Abort()
		return
	}

	solution := db.GetFlowSolutionById(model.Id)
	if solution != nil {
		c.JSON(200, solution.Paths)
		return
	}

	response := make(map[string]interface{})
	solver := src.NewSolver(src.NewGrid(model.Grid))

	paths := solver.Solve(nil, "", nil)
	if paths == nil {
		response["error"] = "Grid not found"
		c.JSON(500, response)
		return
	}

	pathsModel := make([]entities.PathModel, len(paths))
	pathIndex := 0
	for _, path := range paths {
		nodeModels := make([]entities.PathNodeModel, len(path.Nodes))
		for j, node := range path.Nodes {
			nodeModels[j] = entities.PathNodeModel{
				Row:    node.Row,
				Column: node.Col,
				State:  node.State,
			}
		}

		pathsModel[pathIndex] = entities.PathModel{
			Key:   path.Key,
			Color: path.Color,
			Nodes: nodeModels,
		}
		pathIndex++
	}

	solution = &entities.FlowSolution{
		Id:    model.Id,
		Paths: pathsModel,
	}

	db.InsertFlowSolution(solution)
	c.JSON(200, pathsModel)
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header is required"})
			c.Abort()
			return
		}

		tokenString := strings.TrimPrefix(authHeader, "Bearer ")
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(secretKey), nil
		})

		if err != nil || !token.Valid {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			c.Abort()
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token claims"})
			c.Abort()
			return
		}

		// Set claims to context
		c.Set("claims", claims)
		c.Next()
	}
}
