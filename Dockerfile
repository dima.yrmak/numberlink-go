FROM golang:1.12.2

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

RUN go mod download

RUN go get -u github.com/jstemmer/go-junit-report

RUN go test ./src -v | go-junit-report > result.xml

CMD tail -f /dev/null
