package db

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"numberlink/db/entities"
)

func connect() *mongo.Client {
	opts := options.Client().ApplyURI("mongodb+srv://user:s3Wh0jQPDPcEYUhh@cluster0.2myiitb.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0")

	// Create a new client and connect to the server
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		panic(err)
	}

	return client
}

func GetUser(email string) *entities.User {
	client := connect()

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	collection := client.Database("numberlinks").Collection("users")

	var user entities.User
	err := collection.FindOne(context.TODO(), bson.D{{"email", email}}).Decode(&user)
	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil
	}
	if err != nil {
		panic(err)
	}

	return &user
}

func InsertUser(user *entities.User) bool {
	client := connect()
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	collection := client.Database("numberlinks").Collection("users")

	_, err := collection.InsertOne(context.TODO(), user)
	return err == nil
}

func GetFlowById(id int) *entities.Flow {
	client := connect()
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	collection := client.Database("numberlinks").Collection("flows")

	var flow entities.Flow
	err := collection.FindOne(context.TODO(), bson.D{{"id", id}}).Decode(&flow)
	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil
	}
	if err != nil {
		panic(err)
	}

	return &flow
}

func GetFlowByGrid(grid []string) *entities.Flow {
	client := connect()
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	collection := client.Database("numberlinks").Collection("flows")

	var flow entities.Flow
	err := collection.FindOne(context.TODO(), bson.D{{"grid", grid}}).Decode(&flow)
	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil
	}
	if err != nil {
		panic(err)
	}

	return &flow
}

func GetFlowIds() []int {
	client := connect()

	collection := client.Database("numberlinks").Collection("flows")
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	cursor, err := collection.Find(context.TODO(), bson.D{})
	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil
	}
	if err != nil {
		panic(err)
	}
	var flows []entities.Flow

	err = cursor.All(context.TODO(), flows)
	if err != nil {
		return nil
	}

	ids := make([]int, len(flows))
	for i, flow := range flows {
		ids[i] = flow.Id
	}

	return ids
}

func InsertFlow(flow *entities.Flow) int {
	client := connect()

	collection := client.Database("numberlinks").Collection("flows")

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	opt := options.Find().SetSort(bson.D{{"id", -1}}).SetProjection(
		bson.D{{"grid", 0}, {"_id", 0}},
	).SetLimit(1)

	var flows []entities.Flow
	cursor, err := collection.Find(context.TODO(), bson.D{}, opt)
	err = cursor.All(context.TODO(), &flows)
	if err != nil && len(flows) == 0 {
		return -1
	}

	nextId := flows[0].Id
	flow.Id = nextId

	_, err = collection.InsertOne(context.TODO(), flow)
	return nextId
}

func InsertFlowSolution(solution *entities.FlowSolution) bool {
	client := connect()

	collection := client.Database("numberlinks").Collection("flow_solutions")

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	_, err := collection.InsertOne(context.TODO(), solution)
	return err == nil
}

func GetFlowSolutionById(id int) *entities.FlowSolution {
	client := connect()

	collection := client.Database("numberlinks").Collection("flow_solutions")
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	var solution entities.FlowSolution
	err := collection.FindOne(context.TODO(), bson.D{{"flow_id", id}}).Decode(&solution)
	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil
	}
	if err != nil {
		panic(err)
	}

	return &solution
}
