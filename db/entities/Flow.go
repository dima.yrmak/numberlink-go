package entities

type Flow struct {
	Id   int      `bson:"id,omitempty" json:"id"`
	Grid []string `bson:"grid,omitempty" json:"grid"`
}
