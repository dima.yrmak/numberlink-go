package entities

type PathModel struct {
	Key   string          `bson:"key,omitempty" json:"key"`
	Color string          `bson:"color,omitempty" json:"color"`
	Nodes []PathNodeModel `bson:"nodes,omitempty" json:"nodes"`
}

type PathNodeModel struct {
	Row    int `bson:"row,omitempty" json:"row"`
	Column int `bson:"column,omitempty" json:"column"`
	State  int `bson:"state,omitempty" json:"state"`
}
