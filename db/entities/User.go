package entities

type User struct {
	Email    string `bson:"email,omitempty" json:"email"`
	FullName string `bson:"full_name,omitempty" json:"full_name,omitempty"`
	Password string `bson:"password,omitempty" json:"password"`
}
