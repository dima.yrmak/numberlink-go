package entities

type FlowSolution struct {
	Id    int         `bson:"flow_id,omitempty" json:"flow_id"`
	Paths []PathModel `bson:"solution,omitempty" json:"solution"`
}
