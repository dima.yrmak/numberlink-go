package entities

type NumberLinkModel struct {
	Id   int      `json:"id,omitempty"`
	Grid []string `json:"game_grid"`
}
