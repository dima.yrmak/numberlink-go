package src

type Flow struct {
	key                 string
	start               [2]int
	end                 [2]int
	startNeighbors      [][2]int
	endNeighbors        [][2]int
	startNeighborsCount int
	endNeighborsCount   int
}

func NewFlow(key string, startNeighbors [][2]int, endNeighbors [][2]int) *Flow {
	flow := &Flow{}
	flow.key = key
	flow.startNeighbors = startNeighbors
	flow.endNeighbors = endNeighbors
	flow.startNeighborsCount = len(startNeighbors)
	flow.endNeighborsCount = len(endNeighbors)

	return flow
}

func (flow *Flow) SwapFlowStartPointIfNeeded() {
	if flow.startNeighborsCount > flow.endNeighborsCount {
		tempStart := flow.start
		tempNeighbors := flow.startNeighbors

		flow.start = flow.end
		flow.end = tempStart
		flow.startNeighbors = flow.endNeighbors
		flow.startNeighborsCount = flow.endNeighborsCount
		flow.endNeighbors = tempNeighbors
		flow.endNeighborsCount = len(tempNeighbors)
	}
}

func (flow *Flow) GetFlowMinNeighborsCount() int {
	return min(flow.startNeighborsCount, flow.endNeighborsCount)
}
