package src

import "testing"

var board6x6 = []string{
	"123 45",
	"    6 ",
	"  3   ",
	"  4   ",
	"1 6   ",
	"2 5   ",
}

func TestSolver_Solve6x6(t *testing.T) {
	solver := NewSolver(NewGrid(board6x6))

	if solver.Solve(nil, "", nil) == nil {
		t.Error("Solution must be found")
	}
}
