package src

import (
	"slices"
)

type Node struct {
	key       string
	row       int
	col       int
	visited   [][2]int
	neighbors []Node
	prevNode  *Node
}

func NewNode(key string, row int, col int) *Node {
	node := &Node{}
	node.key = key
	node.row = row
	node.col = col

	return node
}

func (node *Node) InitNeighbors(graph map[[2]int][][2]int) {
	neighbors, inGraph := graph[[2]int{node.row, node.col}]
	if !inGraph {
		return
	}

	for _, neighbor := range neighbors {
		node.neighbors = append(node.neighbors, *NewNode(node.key, neighbor[0], neighbor[1]))
	}
}

func (node *Node) AddVisited(nextNode *Node) {
	node.visited = append(node.visited, [2]int{nextNode.row, nextNode.col})
}

func (node *Node) GetNext() *Node {
	for _, neighbor := range node.neighbors {
		if !slices.Contains(node.visited, [2]int{neighbor.row, neighbor.col}) {
			neighbor.prevNode = node
			return &neighbor
		}
	}

	return nil
}

type FlowSolver struct {
	key      string
	flows    *FlowsCollection
	color    string
	grid     *Grid
	start    *Node
	end      *Node
	graph    map[[2]int][][2]int
	path     *Path
	lastNode *Node
}

func NewFlowSolver(grid *Grid, key string, color string, flows *FlowsCollection) *FlowSolver {
	return &FlowSolver{
		key:   key,
		grid:  grid,
		color: color,
		flows: flows,
	}
}

func (solver *FlowSolver) Init(start [2]int, end [2]int) {
	solver.start = NewNode(solver.key, start[0], start[1])
	solver.end = NewNode(solver.key, end[0], end[1])

	solver.path = NewPath(solver.key, solver.color)
	solver.path.flows = solver.flows
	solver.path.grid = solver.grid
	solver.path.Add(start)
}

func (solver *FlowSolver) IsValidFlow() bool {
	return solver.start != nil && solver.end != nil
}

func (solver *FlowSolver) FindSolution() *Path {
	var currentNode *Node

	if solver.lastNode == nil {
		currentNode = solver.start
		currentNode.InitNeighbors(solver.graph)
	} else {
		currentNode = solver.lastNode
	}

	for currentNode != nil {
		nextNode := currentNode.GetNext()
		if nextNode == nil {
			if currentNode.row != solver.end.row && currentNode.col != solver.end.row {
				solver.path.Remove([2]int{currentNode.row, currentNode.col})
			}
			currentNode = currentNode.prevNode
			continue
		}

		currentNode.AddVisited(nextNode)
		if !solver.path.Contains([2]int{nextNode.row, nextNode.col}) {
			if nextNode.row == solver.end.row && nextNode.col == solver.end.col {
				solver.lastNode = currentNode
				pathClone := solver.path.Copy()
				pathClone.Add([2]int{nextNode.row, nextNode.col})
				return pathClone
			}

			solver.path.Add([2]int{nextNode.row, nextNode.col})
			if !solver.path.IsValid() {
				solver.path.Remove([2]int{nextNode.row, nextNode.col})
				continue
			}

			currentNode = nextNode
			currentNode.InitNeighbors(solver.graph)
		}
	}

	return nil
}
