package src

type FlowsCollectionBuilder struct {
	grid           *Grid
	oneNeighbors   []Flow
	twoNeighbors   []Flow
	threeNeighbors []Flow
	fourNeighbors  []Flow
}

func NewFlowsCollectionBuilder(grid *Grid) *FlowsCollectionBuilder {
	return &FlowsCollectionBuilder{
		grid: grid,
	}
}

func (builder *FlowsCollectionBuilder) AddFlowToBuilder(key string, start [2]int, end [2]int) {
	startNeighbors := builder.grid.GetNeighbors(start, key)
	endNeighbors := builder.grid.GetNeighbors(end, key)

	flow := NewFlow(key, startNeighbors, endNeighbors)
	flow.start = start
	flow.end = end

	flow.SwapFlowStartPointIfNeeded()
	minNeighborsCount := flow.GetFlowMinNeighborsCount()
	switch minNeighborsCount {
	case 1:
		builder.oneNeighbors = append(builder.oneNeighbors, *flow)
	case 2:
		builder.twoNeighbors = append(builder.twoNeighbors, *flow)
	case 3:
		builder.threeNeighbors = append(builder.threeNeighbors, *flow)
	case 4:
		builder.fourNeighbors = append(builder.fourNeighbors, *flow)
	}
}

func (builder *FlowsCollectionBuilder) Build() *FlowsCollection {
	cellsByKeys := make(map[string][2]int)
	for _, cell := range builder.grid.InitiallyNotEmptyCells {
		key := string(builder.grid.grid[cell[0]][cell[1]])
		sameCell, found := cellsByKeys[key]
		if found {
			builder.AddFlowToBuilder(key, sameCell, cell)
		} else {
			cellsByKeys[key] = cell
		}
	}

	flows := append(builder.oneNeighbors, builder.twoNeighbors...)
	flows = append(flows, builder.threeNeighbors...)
	flows = append(flows, builder.fourNeighbors...)

	return NewFlowsCollection(flows)
}
