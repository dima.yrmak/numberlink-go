package src

import (
	"testing"
)

func TestGenerateColorsAllUnique(t *testing.T) {
	generator := NewColorGenerator()

	colors := make([]string, 10000)
	for i := 0; i < 10000; i++ {
		colors[i] = generator.Generate()
	}

	if len(colors) != len(UniqueSliceElements(colors)) {
		t.Error("Colors not unique")
	}
}

func TestForSymbol(t *testing.T) {
	generator := NewColorGenerator()

	symbolColor := generator.SymbolColor("1")

	if symbolColor != generator.SymbolColor("1") {
		t.Error("Symbol Color should be the same")
	}

	if generator.SymbolColor("1") != generator.SymbolColor("1") {
		t.Error("Symbol Color should be the same")
	}

	if generator.SymbolColor("1") == generator.SymbolColor("2") {
		t.Error("Different symbols should have different colors")
	}

}

func UniqueSliceElements[T comparable](inputSlice []T) []T {
	uniqueSlice := make([]T, 0, len(inputSlice))
	seen := make(map[T]bool, len(inputSlice))
	for _, element := range inputSlice {
		if !seen[element] {
			uniqueSlice = append(uniqueSlice, element)
			seen[element] = true
		}
	}
	return uniqueSlice
}
