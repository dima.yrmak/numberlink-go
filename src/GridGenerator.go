package src

import (
	"fmt"
	"math"
	"math/rand"
	"slices"
)

func GenerateGrid(size int) (*Grid, error) {
	for {
		// Initialize the board with empty spaces
		board := make([]string, size)
		for i := 0; i < size; i++ {
			board[i] = string(make([]rune, size))
			for j := range board[i] {
				board[i] = replaceAtIndex(board[i], ' ', j)
			}
		}

		// List of empty cells
		var emptyCells [][2]int
		for i := 0; i < size; i++ {
			for j := 0; j < size; j++ {
				emptyCells = append(emptyCells, [2]int{i, j})
			}
		}

		// Generate pairs and place them on the board
		for i := 0; i < size; i++ {
			var start, end [2]int
			tryCount := 10
			for {
				startIndex := rand.Intn(len(emptyCells))
				endIndex := rand.Intn(len(emptyCells))
				start = emptyCells[startIndex]
				end = emptyCells[endIndex]

				tryCount--
				if math.Sqrt(math.Pow(float64(start[0]-end[0]), 2)+math.Pow(float64(start[1]-end[1]), 2)) > 1 {
					break
				}
				if tryCount == 0 {
					return nil, fmt.Errorf("failed to create grid")
				}
			}

			pathKey := rune('A' + i)

			board[start[0]] = replaceAtIndex(board[start[0]], pathKey, start[1])
			board[end[0]] = replaceAtIndex(board[end[0]], pathKey, end[1])
			emptyCells = deleteFromSlice(emptyCells, start)
			emptyCells = deleteFromSlice(emptyCells, end)
		}

		// Ensure that the generated board is solvable
		boardCopy := make([]string, size)
		copy(board, boardCopy)
		grid := NewGrid(board)
		solver := NewSolver(grid)
		if solver.Solve(nil, "", nil) != nil {
			return NewGrid(boardCopy), nil
		}
	}
}

func deleteFromSlice(slice [][2]int, element [2]int) [][2]int {
	index := slices.Index(slice, element)
	if index != -1 {
		return slices.Delete(slice, index, index+1)
	}

	return slice
}
