package src

import (
	"slices"
)

const UNDEFINED_STATE = 0
const HORIZONTAL_STATE = 1
const VERTICAL_STATE = 2
const LEFT_UP_STATE = 3
const RIGHT_UP_STATE = 4
const LEFT_DOWN_STATE = 5
const RIGHT_DOWN_STATE = 6

type PathNode struct {
	prev  *PathNode
	Row   int
	Col   int
	State int
}

func (node *PathNode) UpdateState(point [2]int) {
	if node.prev != nil {
		if node.Row == node.prev.Row && node.Row == point[0] {
			node.State = HORIZONTAL_STATE
		} else if node.Col == node.prev.Col && node.Col == point[1] {
			node.State = VERTICAL_STATE
		} else if node.Row < node.prev.Row || node.Row < point[0] {
			if node.Col < node.prev.Col || node.Col < point[1] {
				node.State = RIGHT_DOWN_STATE
			} else {
				node.State = LEFT_DOWN_STATE
			}
		} else {
			if node.Col < node.prev.Col || node.Col < point[1] {
				node.State = RIGHT_UP_STATE
			} else {
				node.State = LEFT_UP_STATE
			}
		}
	}
}

type Path struct {
	Key   string
	flows *FlowsCollection
	Color string
	path  [][2]int
	Nodes []PathNode
	grid  *Grid
}

func NewPath(key string, color string) *Path {
	path := &Path{}

	path.Key = key
	path.Color = color

	return path
}

func (path *Path) Contains(point [2]int) bool {
	return slices.Contains(path.path, point)
}

func (path *Path) Add(point [2]int) {
	var lastNode *PathNode
	if len(path.Nodes) <= 0 {
		lastNode = nil
	} else {
		lastNode = &path.Nodes[len(path.Nodes)-1]
	}

	if lastNode != nil {
		lastNode.UpdateState(point)
	}

	node := PathNode{}
	node.prev = lastNode
	node.Row = point[0]
	node.Col = point[1]
	node.UpdateState(point)

	path.Nodes = append(path.Nodes, node)
	path.path = append(path.path, point)

	if path.grid != nil {
		path.grid.AddPoint(path.Key, point)
	}
}

func (path *Path) Remove(point [2]int) {
	elementIndex := slices.Index(path.path, point)
	path.path = slices.Delete(path.path, elementIndex, elementIndex+1)
	path.Nodes = slices.Delete(path.Nodes, elementIndex, elementIndex+1)

	if path.grid != nil {
		path.grid.RemovePoint(point)
	}
}

func (path *Path) GetLastPoint() [2]int {
	return path.path[len(path.path)-1]
}

func (path *Path) IsReachable(point [2]int) bool {
	stack := make([][2]int, 0)
	stack = append(stack, point)
	visited := make([][2]int, 0)
	visited = append(visited, point)

	for len(stack) != 0 {
		stackLength := len(stack) - 1
		current := stack[stackLength]
		stack = slices.Delete(stack, stackLength, stackLength+1)

		nodeNeighbors := path.grid.GetNeighbors(current, "")
		for _, neighbor := range nodeNeighbors {
			neighborChar := rune(path.grid.grid[neighbor[0]][neighbor[1]])
			if !slices.Contains(visited, neighbor) {
				visited = append(visited, neighbor)
				if neighborChar == ' ' {
					stack = append(stack, neighbor)
				}
			}
		}
	}

	lastPathPoint := path.GetLastPoint()
	for _, flow := range path.flows.flows {
		if flow.key == path.Key && slices.Contains(visited, flow.end) && slices.Contains(visited, lastPathPoint) {
			return true
		} else if slices.Contains(visited, flow.start) && slices.Contains(visited, flow.end) {
			return true
		}
	}

	return false
}

func (path *Path) HasZigZag() bool {
	for _, node := range path.Nodes {
		for _, delta := range [][2]int{{1, -1}, {-1, 1}, {-1, -1}, {1, 1}} {
			rowDelta, colDelta := delta[0], delta[1]

			neighbors := [][2]int{{node.Row, node.Col}, {node.Row + rowDelta, node.Col + colDelta}, {node.Row + rowDelta, node.Col}, {node.Row, node.Col + colDelta}}

			for _, neighbor := range neighbors {
				if !path.Contains(neighbor) {
					return false
				}
			}
		}
	}

	return true
}

func (path *Path) IsOtherFlowsBlocked() bool {
	for _, flow := range path.flows.flows {
		if flow.key == path.Key {
			continue
		}

		if path.isAllNodesInPath(flow.startNeighbors) || path.isAllNodesInPath(flow.endNeighbors) {
			return true
		}
	}

	return false
}

func (path *Path) isAllNodesInPath(nodes [][2]int) bool {
	nodeCount := len(nodes)

	blockedNodesCount := 0
	for _, startNode := range nodes {
		if path.grid.CharAt(startNode[0], startNode[1]) != ' ' {
			blockedNodesCount++
		}
	}

	return blockedNodesCount == nodeCount
}

func (path *Path) IsValid() bool {
	if path.HasZigZag() {
		return false
	}

	if path.IsOtherFlowsBlocked() {
		return false
	}

	for _, cell := range path.grid.EmptyCells {
		if !path.IsReachable(cell) {
			return false
		}
	}

	return true
}

func (path *Path) Copy() *Path {
	newPath := NewPath(path.Key, path.Color)
	newPath.flows = path.flows.Copy()
	newPath.grid = path.grid.Copy()
	newPath.path = make([][2]int, len(path.path))
	copy(newPath.path, path.path)
	newPath.Nodes = make([]PathNode, len(path.Nodes))
	copy(newPath.Nodes, path.Nodes)

	return newPath
}
