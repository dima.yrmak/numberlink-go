package src

type FlowsCollection struct {
	flows     map[string]Flow
	flowKeys  []string
	keysOrder map[string]string
}

func NewFlowsCollection(flows []Flow) *FlowsCollection {
	flowsCollection := &FlowsCollection{}

	flowsDict := make(map[string]Flow)

	keys := make([]string, 0, len(flows))
	keysOrder := make(map[string]string)

	prevKey := ""
	for _, flow := range flows {
		flowsDict[flow.key] = flow

		keys = append(keys, flow.key)
		keysOrder[prevKey] = flow.key
		prevKey = flow.key
	}

	flowsCollection.flows = flowsDict
	flowsCollection.flowKeys = keys
	flowsCollection.keysOrder = keysOrder

	return flowsCollection
}

func (collection *FlowsCollection) GetNextKey(key string) (string, bool) {
	flowsCount := len(collection.keysOrder)

	if flowsCount == 0 {
		return "", false
	}

	key, found := collection.keysOrder[key]
	return key, found
}

func (collection *FlowsCollection) CopyFlowsCollectionWithoutKey(key string) *FlowsCollection {
	flows := make([]Flow, 0)
	for _, flow := range collection.flows {
		if flow.key != key {
			flows = append(flows, flow)
		}
	}

	return NewFlowsCollection(flows)
}

func (collection *FlowsCollection) Copy() *FlowsCollection {
	flows := make([]Flow, 0)
	for _, flow := range collection.flows {
		flows = append(flows, flow)
	}

	return NewFlowsCollection(flows)
}
