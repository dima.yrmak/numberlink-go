package src

import (
	"slices"
)

type GraphBuilder struct {
	key   string
	grid  Grid
	start [2]int
	end   [2]int
}

func NewGraphBuilder(grid Grid, key string, start [2]int, end [2]int) *GraphBuilder {
	graphBuilder := &GraphBuilder{}

	graphBuilder.grid = grid
	graphBuilder.start = start
	graphBuilder.end = end
	graphBuilder.key = key

	return graphBuilder
}

func (builder *GraphBuilder) AddToFromNode(toFromNodes map[[2]int][][2]int, node [2]int, nextNode [2]int) {
	nodes, found := toFromNodes[nextNode]
	if found {
		nodes = append(nodes, node)
	} else {
		nodes = make([][2]int, 1)
		nodes[0] = node
		toFromNodes[nextNode] = nodes
	}
}

func (builder *GraphBuilder) Build() map[[2]int][][2]int {
	graph := make(map[[2]int][][2]int)
	toFromNodes := make(map[[2]int][][2]int)

	stack := make([][2]int, 0)
	stack = append(stack, builder.start)

	for len(stack) != 0 {
		lastNodeIndex := len(stack) - 1
		node := stack[lastNodeIndex]
		stack = append(stack[:lastNodeIndex], stack[lastNodeIndex+1:]...)

		neighbors := builder.grid.GetNeighbors(node, builder.key)

		nodeNeighbors := make([][2]int, 0)
		for _, neighbor := range neighbors {
			neighborNeighbors := builder.grid.GetNeighbors(neighbor, builder.key)
			neighborNeighborsCount := 0
			for _, neighborNeighbor := range neighborNeighbors {
				if neighborNeighbor[0] == node[0] && neighborNeighbor[1] == node[1] {
					continue
				}
				neighborNeighborsCount++
			}

			if neighborNeighborsCount == 1 ||
				(neighbor[0] == builder.end[0] && neighbor[1] == builder.end[1]) {
				nodeNeighbors = slices.Insert(nodeNeighbors, 0, neighbor)
			} else {
				nodeNeighbors = append(nodeNeighbors, neighbor)
			}

			builder.AddToFromNode(toFromNodes, node, neighbor)

			_, inGraph := graph[neighbor]
			if !inGraph {
				stack = append(stack, neighbor)
			}
		}

		_, inGraph := graph[node]
		if !inGraph && len(nodeNeighbors) != 0 {
			graph[node] = nodeNeighbors
		}
	}

	return graph
}
