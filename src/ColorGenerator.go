package src

import (
	"fmt"
	"math/rand"
	"slices"
)

type ColorGenerator struct {
	createdColors []string
	symbolColors  map[string]string
}

func NewColorGenerator() *ColorGenerator {
	return &ColorGenerator{
		symbolColors: make(map[string]string),
	}
}

func (generator *ColorGenerator) Generate() string {
	color := "#"
	for i := 0; i < 3; i++ {
		hex := fmt.Sprintf("%x", rand.Intn(255))
		if len(hex) == 1 {
			hex = "0" + hex
		}
		color += hex
	}

	if slices.Contains(generator.createdColors, color) {
		return generator.Generate()
	} else {
		generator.createdColors = append(generator.createdColors, color)
		return color
	}
}

func (generator *ColorGenerator) SymbolColor(symbol string) string {
	color, found := generator.symbolColors[symbol]
	if found {
		return color
	}

	color = generator.Generate()
	generator.symbolColors[symbol] = color

	return color
}
