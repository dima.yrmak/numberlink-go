package src

type Solver struct {
	grid           *Grid
	results        []*Path
	colorGenerator *ColorGenerator
	flows          *FlowsCollection
}

func NewSolver(grid *Grid) *Solver {
	return &Solver{
		grid:           grid,
		colorGenerator: NewColorGenerator(),
		flows:          NewFlowsCollectionBuilder(grid).Build(),
	}
}

func (solver *Solver) Solve(grid *Grid, key string, paths map[string]*Path) map[string]*Path {
	if grid == nil {
		grid = solver.grid
	}
	if paths == nil || len(paths) == 0 {
		paths = make(map[string]*Path)
		key, _ = solver.flows.GetNextKey("")
	}

	flow := solver.flows.flows[key]
	flowsWithoutKey := solver.flows
	for pathKey := range paths {
		flowsWithoutKey = flowsWithoutKey.CopyFlowsCollectionWithoutKey(pathKey)
	}

	for _, flowFromDict := range flowsWithoutKey.flows {
		startNeighbors := make([][2]int, 0)
		for _, neigh := range flowFromDict.startNeighbors {
			if grid.CharAt(neigh[0], neigh[1]) == ' ' {
				startNeighbors = append(startNeighbors, neigh)
			}
		}
		flowFromDict.startNeighbors = startNeighbors
		flowFromDict.startNeighborsCount = len(startNeighbors)

		endNeighbors := make([][2]int, 0)
		for _, neigh := range flowFromDict.endNeighbors {
			if grid.CharAt(neigh[0], neigh[1]) == ' ' {
				endNeighbors = append(endNeighbors, neigh)
			}
		}
		flowFromDict.endNeighbors = endNeighbors
		flowFromDict.endNeighborsCount = len(endNeighbors)
	}

	flowSolver := NewFlowSolver(grid, key, solver.colorGenerator.SymbolColor(key), flowsWithoutKey)
	flowSolver.Init(flow.start, flow.end)
	if !flowSolver.IsValidFlow() {
		panic("Flow with Key " + key + " is not valid")
		return nil
	}

	flowSolver.graph = NewGraphBuilder(*grid, key, flow.start, flow.end).Build()

	for true {
		path := flowSolver.FindSolution()
		if path == nil {
			return nil
		}

		nextGrid := solver.CreateGridWithPath(grid, path, key)
		paths[key] = path
		nextKey, hasNext := solver.flows.GetNextKey(key)
		if !hasNext {
			if nextGrid.IsSolved() {
				return paths
			} else {
				return nil
			}
		}

		if solver.Solve(nextGrid, nextKey, paths) != nil {
			return paths
		} else {
			delete(paths, key)
		}
	}

	return nil
}

func (solver *Solver) CreateGridWithPath(grid *Grid, path *Path, key string) *Grid {
	clone := grid.Copy()
	for _, node := range path.Nodes {
		clone.AddPoint(key, [2]int{node.Row, node.Col})
	}

	return clone
}
