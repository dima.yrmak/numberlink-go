package src

import (
	"slices"
	"testing"
)

var board = []string{
	"1 2 3",
	"  4 5",
	"     ",
	" 2 3 ",
	" 145 ",
}

var solvedBoard = []string{
	"12233",
	"12435",
	"12435",
	"12435",
	"11455",
}

func TestDefineNonEmptyCells(t *testing.T) {
	expected := [][2]int{{0, 0}, {0, 2}, {0, 4}, {1, 2}, {1, 4}, {3, 1}, {3, 3}, {4, 1}, {4, 2}, {4, 3}}

	grid := NewGrid(board)

	if len(expected) != len(grid.InitiallyNotEmptyCells) {
		t.Error("Not all non-empty point found")
	}

	for _, cell := range grid.InitiallyNotEmptyCells {
		if !slices.Contains(expected, cell) {
			t.Errorf("Cell %v should contain %v", cell, expected)
		}
	}
}

func TestIsSolved(t *testing.T) {
	grid := NewGrid(board)

	if grid.IsSolved() {
		t.Error("Grid should not be solved")
	}

	grid = NewGrid(solvedBoard)

	if !grid.IsSolved() {
		t.Error("Grid should be solved")
	}
}

func TestAddPoint(t *testing.T) {
	grid := NewGrid(board)

	grid.AddPoint("g", [2]int{1, 1})

	if grid.CharAt(1, 1) != 'g' {
		t.Error("Point not added to grid")
	}
}

func TestRemovePoint(t *testing.T) {
	grid := NewGrid(board)

	grid.RemovePoint([2]int{0, 0})

	if grid.CharAt(0, 0) != ' ' {
		t.Error("Point not removed from grid")
	}
}

func TestGetNeighborsNoKey(t *testing.T) {
	cornerPointNeighborsExpected := [][2]int{{1, 0}, {0, 1}}
	edgePointNeighborsExpected := [][2]int{{1, 1}, {0, 0}, {0, 2}}
	pointNeighborsExpected := [][2]int{{0, 1}, {2, 1}, {1, 0}, {1, 2}}

	grid := NewGrid(board)

	cornerPointNeighbors := grid.GetNeighbors([2]int{0, 0}, "")
	edgePointNeighbors := grid.GetNeighbors([2]int{0, 1}, "")
	pointNeighbors := grid.GetNeighbors([2]int{1, 1}, "")

	if len(cornerPointNeighborsExpected) != len(cornerPointNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range cornerPointNeighbors {
		if !slices.Contains(cornerPointNeighborsExpected, cell) {
			t.Error("Invalid corner neighbor found")
		}
	}

	if len(edgePointNeighborsExpected) != len(edgePointNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range edgePointNeighbors {
		if !slices.Contains(edgePointNeighborsExpected, cell) {
			t.Error("Invalid edge neighbor found")
		}
	}

	if len(pointNeighborsExpected) != len(pointNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range pointNeighbors {
		if !slices.Contains(pointNeighborsExpected, cell) {
			t.Error("Invalid corner neighbor found")
		}
	}
}

func TestGetNeighborsWithKey(t *testing.T) {
	cornerPointNeighborsExpected := [][2]int{{1, 0}, {0, 1}}
	cornerPointWithKeyNeighborNeighborsExpected := [][2]int{{3, 0}, {4, 1}}
	cornerPointWithNotKeyNeighborNeighborsExpected := [][2]int{{3, 4}}
	edgePointWithNotOnSidesExpected := [][2]int{{1, 3}}

	grid := NewGrid(board)

	cornerPointNeighbors := grid.GetNeighbors([2]int{0, 0}, "1")
	cornerPointWithKeyNeighborNeighbors := grid.GetNeighbors([2]int{4, 0}, "1")
	cornerPointWithNotKeyNeighborNeighbors := grid.GetNeighbors([2]int{4, 4}, "1")
	edgePointWithNotOnSides := grid.GetNeighbors([2]int{0, 3}, "1")

	if len(cornerPointNeighborsExpected) != len(cornerPointNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range cornerPointNeighbors {
		if !slices.Contains(cornerPointNeighborsExpected, cell) {
			t.Error("Invalid corner neighbor found")
		}
	}

	if len(cornerPointWithKeyNeighborNeighborsExpected) != len(cornerPointWithKeyNeighborNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range cornerPointWithKeyNeighborNeighbors {
		if !slices.Contains(cornerPointWithKeyNeighborNeighborsExpected, cell) {
			t.Error("Invalid edge neighbor found")
		}
	}

	if len(cornerPointWithNotKeyNeighborNeighborsExpected) != len(cornerPointWithNotKeyNeighborNeighbors) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range cornerPointWithNotKeyNeighborNeighbors {
		if !slices.Contains(cornerPointWithNotKeyNeighborNeighborsExpected, cell) {
			t.Error("Invalid corner neighbor found")
		}
	}

	if len(edgePointWithNotOnSidesExpected) != len(edgePointWithNotOnSides) {
		t.Error("Not all corner neighbors found")
	}
	for _, cell := range edgePointWithNotOnSides {
		if !slices.Contains(edgePointWithNotOnSidesExpected, cell) {
			t.Error("Invalid corner neighbor found")
		}
	}
}
