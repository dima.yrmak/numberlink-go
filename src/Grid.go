package src

import (
	"fmt"
	"strings"
)

func printRow(length int) {
	rowString := "-"
	for i := 0; i < length; i++ {
		rowString += "--"
	}
	fmt.Println(rowString)
}

type Grid struct {
	grid                   []string
	EmptyCells             [][2]int
	rows                   int
	cols                   int
	InitiallyNotEmptyCells [][2]int
}

func NewGrid(gameBoard []string) *Grid {
	grid := &Grid{}
	if gameBoard != nil && len(gameBoard) > 0 && len(gameBoard[0]) > 0 {
		grid.grid = make([]string, len(gameBoard))
		for i, row := range gameBoard {
			grid.grid[i] = row
			for j := range row {
				col := string(row[j])
				if col != " " {
					grid.InitiallyNotEmptyCells = append(grid.InitiallyNotEmptyCells, [2]int{i, j})
				} else {
					grid.EmptyCells = append(grid.EmptyCells, [2]int{i, j})
				}
			}
		}
	} else {
		grid.grid = gameBoard
	}
	grid.rows = len(grid.grid)
	grid.cols = len(grid.grid[0])
	return grid
}

func (g *Grid) Print() {
	for i, row := range g.grid {
		rowString := ""
		if i == 0 {
			printRow(len(g.grid[i]))
		}

		rowString = "|"
		for _, col := range row {
			rowString += string(col) + "|"
		}

		fmt.Println(rowString)

		if i == g.rows-1 {
			printRow(len(g.grid[i]))
		}
	}
}

func (g *Grid) IsSolved() bool {
	for _, row := range g.grid {
		for _, col := range row {
			if col == ' ' {
				return false
			}
		}
	}
	return true
}

func (g *Grid) GetNeighbors(point [2]int, key string) [][2]int {
	rowIndex, colIndex := point[0], point[1]

	neighbors := [][2]int{{rowIndex - 1, colIndex}, {rowIndex + 1, colIndex}, {rowIndex, colIndex - 1}, {rowIndex, colIndex + 1}}

	validNeighbors := make([][2]int, 0)
	for _, n := range neighbors {
		if !g.IsValidCell(n, key) {
			continue
		}
		validNeighbors = append(validNeighbors, n)
	}

	return validNeighbors
}

func (g *Grid) IsValidCell(nextPoint [2]int, key string) bool {
	if nextPoint[0] < 0 || nextPoint[0] >= g.rows || nextPoint[1] < 0 || nextPoint[1] >= g.cols {
		return false
	}

	nextPointChar := string(g.grid[nextPoint[0]][nextPoint[1]])
	if key == "" {
		return true
	}

	return nextPointChar == key || nextPointChar == " "
}

func (g *Grid) AddPoint(key string, point [2]int) {
	if g.IsValidCell(point, key) {
		row, col := point[0], point[1]

		var keyRune rune
		if len(key) == 0 {
			keyRune = ' '
		} else {
			keyRune = []rune(key)[0]
		}

		g.grid[row] = replaceAtIndex(g.grid[row], keyRune, col)
		emptyCells := make([][2]int, 0)
		for _, emptyCell := range g.EmptyCells {
			if emptyCell[0] == row && emptyCell[1] == col {
				continue
			}
			emptyCells = append(emptyCells, emptyCell)
		}
		g.EmptyCells = emptyCells
	}
}

func (g *Grid) RemovePoint(point [2]int) {
	rowIndex, colIndex := point[0], point[1]
	if 0 <= rowIndex && rowIndex < g.rows && 0 <= colIndex && colIndex < g.cols {
		g.grid[rowIndex] = replaceAtIndex(g.grid[rowIndex], ' ', colIndex)
		found := false
		for _, cell := range g.EmptyCells {
			if cell[0] == rowIndex && cell[1] == colIndex {
				found = true
				break
			}
		}
		if !found {
			g.EmptyCells = append(g.EmptyCells, [2]int{rowIndex, colIndex})
		}
	}
}

func (grid *Grid) Copy() *Grid {
	newGridBoard := make([]string, 0)
	for _, row := range grid.grid {
		newGridBoard = append(newGridBoard, strings.Clone(row))
	}

	newGrid := NewGrid(newGridBoard)
	newGrid.EmptyCells = grid.EmptyCells

	return newGrid
}

func (grid *Grid) CharAt(row int, col int) rune {
	return rune(grid.grid[row][col])
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}
