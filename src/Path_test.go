package src

import "testing"

var pathGameBoard = []string{
	"         ",
	"12     3 ",
	"         ",
	" 4   52  ",
	"  1  4   ",
	"  6      ",
	"  3      ",
	"        6",
	"        5",
}

func TestPath_Add(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{1, 1})
	path.Add([2]int{1, 2})
	path.Add([2]int{2, 2})

	if path.Nodes[0].prev != nil {
		t.Error("path.Nodes[0].prev should be nil")
	}

	if path.Nodes[1].prev == nil || (path.Nodes[1].prev.Col != path.Nodes[0].Col && path.Nodes[1].prev.Row != path.Nodes[0].Row) {
		t.Error("path.Nodes[1].prev should be path.Nodes[0]")
	}

	if path.Nodes[2].prev == nil || (path.Nodes[2].prev.Col != path.Nodes[1].Col && path.Nodes[2].prev.Row != path.Nodes[1].Row) {
		t.Error("path.Nodes[2].prev should be path.Nodes[1]")
	}

	if path.Nodes[0].State != UNDEFINED_STATE {
		t.Error("path.Nodes[0].State should be UNDEFINED_STATE")
	}

	if path.Nodes[1].State != LEFT_DOWN_STATE {
		t.Error("path.Nodes[1].State should be LEFT_DOWN_STATE")
	}

	if path.Nodes[2].State != VERTICAL_STATE {
		t.Error("path.Nodes[2].State should be Vertical STATE")
	}

	if grid.CharAt(1, 1) != []rune(path.Key)[0] {
		t.Error("grid.CharAt(1,1) != []rune(path.Key)[0]")
	}

	if grid.CharAt(1, 2) != []rune(path.Key)[0] {
		t.Error("grid.CharAt(1,2) != []rune(path.Key)[0]")
	}

	if grid.CharAt(2, 2) != []rune(path.Key)[0] {
		t.Error("grid.CharAt(2,2) != []rune(path.Key)[0]")
	}

	if len(path.Nodes) != 3 {
		t.Error("len(path.Nodes) != 3")
	}

	if len(path.path) != 3 {
		t.Error("len(path.path) != 3")
	}
}

func TestPath_Remove(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{1, 1})
	path.Add([2]int{1, 2})

	path.Remove([2]int{1, 2})

	if grid.CharAt(1, 2) != ' ' {
		t.Error("grid.CharAt(1,2) != ' ")
	}

	if len(path.Nodes) != 1 {
		t.Error("len(path.Nodes) != 1")
	}

	if len(path.path) != 1 {
		t.Error("len(path.path) != 1")
	}
}

func TestPath_Contains(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{1, 1})
	path.Add([2]int{1, 2})

	if !path.Contains([2]int{1, 1}) {
		t.Error("path.Contains([2]int{1, 1}) should be true")
	}

	if path.Contains([2]int{2, 2}) {
		t.Error("path.Contains([2]int{2, 2}) should be false")
	}
}

func TestPath_GetLastPoint(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{1, 1})
	path.Add([2]int{1, 2})

	lastPoint := path.GetLastPoint()
	if lastPoint[0] != 1 && lastPoint[1] != 2 {
		t.Error("path.GetLastPoint() should be (1, 2)")
	}

	path.Remove([2]int{1, 2})

	lastPoint = path.GetLastPoint()
	if lastPoint[0] != 1 && lastPoint[1] != 1 {
		t.Error("path.GetLastPoint() should be (1, 1)")
	}

	path.Add([2]int{0, 1})
	path.Add([2]int{0, 2})

	lastPoint = path.GetLastPoint()
	if lastPoint[0] != 0 && lastPoint[1] != 2 {
		t.Error("path.GetLastPoint() should be (0, 2)")
	}
}

func TestPath_IsOtherFlowsBlocked(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("5", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{8, 8})
	path.Add([2]int{8, 7})
	path.Add([2]int{7, 7})
	path.Add([2]int{6, 7})

	if path.IsOtherFlowsBlocked() {
		t.Error("path.IsOtherFlowsBlocked() should be false")
	}
	path.Add([2]int{6, 8})
	if !path.IsOtherFlowsBlocked() {
		t.Error("path.IsOtherFlowsBlocked() should be true")
	}
}

func TestPath_IsReachable(t *testing.T) {
	grid := NewGrid(pathGameBoard)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{1, 1})
	path.Add([2]int{0, 1})
	path.Add([2]int{0, 2})
	path.Add([2]int{0, 3})
	path.Add([2]int{1, 3})
	path.Add([2]int{2, 3})
	path.Add([2]int{2, 2})
	path.Add([2]int{2, 1})

	if path.IsReachable([2]int{0, 0}) {
		t.Error("path.IsReachable([2]int{0, 0}) should be false")
	}

	if path.IsReachable([2]int{1, 2}) {
		t.Error("path.IsReachable([2]int{1, 2}) should be false")
	}

	if !path.IsReachable([2]int{3, 2}) {
		t.Error("path.IsReachable([2]int{1, 2}) should be true")
	}
}

var pathGameBoardWithHole = []string{
	"          ",
	"          ",
	"12     3  ",
	"          ",
	" 4   52   ",
	"  1  4    ",
	"  6       ",
	"  3       ",
	"        6 ",
	"        5 ",
}

func TestPath_isReachableHoles(t *testing.T) {
	grid := NewGrid(pathGameBoardWithHole)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(grid).Build()

	path.Add([2]int{2, 1})
	path.Add([2]int{1, 1})
	path.Add([2]int{0, 1})
	path.Add([2]int{0, 2})
	path.Add([2]int{0, 3})
	path.Add([2]int{0, 4})
	path.Add([2]int{1, 4})
	path.Add([2]int{2, 4})
	path.Add([2]int{3, 4})
	path.Add([2]int{4, 4})
	path.Add([2]int{4, 3})
	path.Add([2]int{4, 2})
	path.Add([2]int{3, 2})
	path.Add([2]int{2, 2})

	if path.IsReachable([2]int{1, 2}) {
		t.Error("path.IsReachable([2]int{1,2}) should be false")
	}

	if path.IsReachable([2]int{1, 3}) {
		t.Error("path.IsReachable([2]int{1,3}) should be false")
	}

	if !path.IsReachable([2]int{3, 5}) {
		t.Error("path.IsReachable([2]int{3,5}) should be true")
	}
}

var pathBoard2Unsolved = []string{
	"123 45",
	"    6 ",
	"  3   ",
	"  4   ",
	"1 6   ",
	"2 5   ",
}

var pathBoard2 = []string{
	"123445",
	"123465",
	"1234 5",
	"1244 5",
	"126  5",
	"225555",
}

func TestPath_isReachableFullPath(t *testing.T) {
	grid := NewGrid(pathBoard2)

	path := NewPath("2", "#000")
	path.grid = grid
	path.flows = NewFlowsCollectionBuilder(NewGrid(pathBoard2Unsolved)).Build()

	path.Add([2]int{1, 4})

	if !path.IsReachable([2]int{2, 4}) {
		t.Error("path.IsReachable([2]int{2, 4}) should be true")
	}

	if !path.IsReachable([2]int{3, 4}) {
		t.Error("path.IsReachable([2]int{3, 4}) should be true")
	}

	if !path.IsReachable([2]int{4, 4}) {
		t.Error("path.IsReachable([2]int{4, 4}) should be true")
	}

	if !path.IsReachable([2]int{4, 3}) {
		t.Error("path.IsReachable([2]int{4, 3}) should be true")
	}
}
